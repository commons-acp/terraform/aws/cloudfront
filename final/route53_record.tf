
resource "aws_route53_record" "record_app" {
  depends_on = [
    aws_cloudfront_distribution.main ,
    aws_cloudfront_distribution.main-lambda-edge
  ]
  provider = aws-main
  zone_id  = var.zone_id
  name     = var.fqdn
  type     = "A"

  alias {
    name    = local.cf_domain_name
    zone_id = local.cf_hosted_zone_id
    evaluate_target_health = false
  }
}
