locals {
  cf_domain_name     = concat(
  aws_cloudfront_distribution.main-lambda-edge.*.domain_name,
  aws_cloudfront_distribution.main.*.domain_name,
  )[0]
  cf_hosted_zone_id  = concat(
  aws_cloudfront_distribution.main-lambda-edge.*.hosted_zone_id,
  aws_cloudfront_distribution.main.*.hosted_zone_id,
  )[0]
}