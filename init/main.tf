data "aws_region" "main" {
  provider = aws-main
}

terraform {
  required_version = ">= 0.12"

  required_providers {
    aws-cloudfront = {
      source  = "hashicorp/aws"
      version = "~> 2.70.0"
    }
    aws-main = {
      source  = "hashicorp/aws"
      version = "~> 2.70.0"
    }
  }

}