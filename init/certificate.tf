# ACM Certificate generation
resource "aws_acm_certificate" "cert_app" {
  provider          = aws-cloudfront
  domain_name       = var.fqdn
  validation_method = "DNS"
}